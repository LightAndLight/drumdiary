{-# LANGUAGE OverloadedStrings #-}

module Handler.Diary where

import           Import

getDiaryR :: Handler Html
getDiaryR = redirect NewEntryR
