{-# LANGUAGE OverloadedStrings #-}

module Handler.Exercises where

import           Database.Esqueleto
import           Import                     hiding (Value, on, (==.))
import qualified Yesod.Form.Bulma.Fields    as B
import           Yesod.Form.Bulma.Functions

exerciseForm :: AForm Handler Exercise
exerciseForm
  = Exercise
    <$> areq textField (B.bulmaInput ("Name" :: Text)) Nothing
    <*> (unTextarea <$> areq textareaField (B.bulmaTextarea ("Description" :: Text)) Nothing)

displayExercisesPage :: Bool -> Widget -> Handler Html
displayExercisesPage added exerciseFormWidget
  = defaultLayout $ do
      setTitle "Exercises"
      $(widgetFile "exercises")

getExercisesR :: Handler Html
getExercisesR = do
  (exerciseFormWidget, _) <- generateFormPost (renderBulma exerciseForm)
  displayExercisesPage False exerciseFormWidget

postExercisesR :: Handler Html
postExercisesR = do
  ((res, exerciseFormWidget), _) <- runFormPost (renderBulma exerciseForm)
  case res of
    FormSuccess exercise -> do
      void . runDB . insert $ exercise
      displayExercisesPage True exerciseFormWidget
    _ -> displayExercisesPage False exerciseFormWidget
