{-# language OverloadedStrings #-}

module Handler.Diary.Entries where

import           Database.Esqueleto
import           Import             hiding ((==.), on, Value)

data EntryRow
  = EntryRow
  { erExerciseName :: Text
  , erEntry :: Entity Entry
  , erDuration :: Maybe Int
  , erMeasures :: Maybe Measure
  , erSubdivision :: Text
  }

entryWidget :: Widget -> EntryRow -> Widget
entryWidget deleteInput (EntryRow name (Entity _ entry) duration' measures subdivision)
  = let date = formatTime defaultTimeLocale "%s" $ entryDate entry
        duration = show <$> duration'
        bpm = show $ entryBpm entry
        mcount = show . measureCount <$> measures
        mlength = show . measureLength <$> measures
    in $(widgetFile "diary/entries/entry-row")

getEntries :: Handler [EntryRow]
getEntries = fmap toEntryRow <$> runDB
  (select $ from $ \(ex `InnerJoin` entry `InnerJoin` subdivision `LeftOuterJoin` measure `LeftOuterJoin` duration) -> do
    on (duration ?. DurationEntryId ==. just (entry ^. EntryId))
    on (measure ?. MeasureEntryId ==. just (entry ^. EntryId))
    on (subdivision ^. SubdivisionId ==. entry ^. EntrySubdivision)
    on (ex ^. ExerciseId ==. entry ^. EntryExercise)
    orderBy [desc (entry ^. EntryDate)]
    return (ex ^. ExerciseName, entry, duration ?. DurationValue, measure, subdivision ^. SubdivisionDisplay))
  where
    toEntryRow (Value name, entry, Value duration, measure, Value sub)
      = EntryRow name entry duration (entityVal <$> measure) sub

entriesForm :: [EntryRow] -> Html -> MForm Handler (FormResult [Key Entry], Widget)
entriesForm entries fragment = do
  deleteCheckboxes <- for entries $ \_ -> mreq checkBoxField "" Nothing
  let entryWidgets = uncurry entryWidget <$> zip (fvInput . snd <$> deleteCheckboxes) entries
  let widget = $(widgetFile "diary/entries/entries-table")
  let entriesToDelete = fmap fst . filter snd . zip (entityKey . erEntry <$> entries) <$> sequenceA (fmap fst deleteCheckboxes)
  return (entriesToDelete, widget)

displayEntriesPage :: Handler Html
displayEntriesPage = do
  entryRows <- getEntries
  (entries, _) <- generateFormPost (entriesForm entryRows)
  Just currentRoute <- getCurrentRoute
  let diaryTabs = $(whamletFile "templates/diary/diary-tabs.hamlet")
  defaultLayout $ do
    setTitle "Entries"
    addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"
    $(widgetFile "diary/entries/entries")

getEntriesR :: Handler Html
getEntriesR = displayEntriesPage