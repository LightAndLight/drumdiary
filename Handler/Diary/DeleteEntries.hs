{-# LANGUAGE OverloadedStrings #-}

module Handler.Diary.DeleteEntries where

import           Import                hiding (delete)

import           Handler.Diary.Entries (entriesForm, getEntries)

postDeleteEntriesR :: Handler Html
postDeleteEntriesR = do
  entries <- getEntries
  ((res, _), _) <- runFormPost (entriesForm entries)
  case res of
    FormSuccess entryKeys -> do
      runDB $ deleteCascadeWhere [EntryId <-. entryKeys]
      redirect EntriesR
    _ -> redirect EntriesR
