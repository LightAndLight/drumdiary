{-# language OverloadedStrings #-}

module Handler.Diary.Progress where

import           Database.Esqueleto
import           Import             hiding ((==.), on, Value)
import Data.Maybe
import Yesod.ChartJs
import Control.Lens ((.~), (&), ix)
import Prelude (read)

data ProgressPoint
  = ProgressPoint
  { ppDate :: UTCTime
  , ppBpm :: Int
  , ppHitsPerBeat :: Int
  , ppDuration :: Maybe Int
  , ppMeasure :: Maybe Measure
  }

progressForm :: Html -> MForm Handler (FormResult (Key Exercise), Widget)
progressForm fragment = do
  (exRes, exView) <- mreq
    (selectField $ optionsPersistKey [] [Asc ExerciseId] exerciseName)
    ("Exercise" { fsName = Just "exercise" })
    Nothing
  let widget =
        [whamlet|
          #{fragment}
          <p .control .has-addons>
            <span .select>
              ^{fvInput exView}
            <button .button .is-primary>View Progress
        |]
  return (exRes, widget)

getEntriesForExercise :: Key Exercise -> Handler [ProgressPoint]
getEntriesForExercise ex = fmap toProgressPoint <$> runDB
  (select $ from $ \(entry `InnerJoin` subdivision `LeftOuterJoin` measure `LeftOuterJoin` duration) -> do
    on (duration ?. DurationEntryId ==. just (entry ^. EntryId))
    on (measure ?. MeasureEntryId ==. just (entry ^. EntryId))
    on (subdivision ^. SubdivisionId ==. entry ^. EntrySubdivision)
    where_ (val ex ==. entry ^. EntryExercise)
    orderBy [desc (entry ^. EntryDate)]
    return (entry ^. EntryDate, entry ^. EntryBpm, subdivision ^. SubdivisionHitsPerBeat, duration ?. DurationValue, measure))
  where
    toProgressPoint (Value date, Value bpm, Value hitsPerBeat, Value duration, measure)
      = ProgressPoint date bpm hitsPerBeat duration (entityVal <$> measure)

exerciseProgressChart :: [ProgressPoint] -> Widget
exerciseProgressChart entries = chart $
  (simpleLineChart (fmap processEntry entries) (Color 0 128 128 1) (Color 0 0 0 0) False)
    & chartOptions . coScales . soXAxes .~
      [ defaultAxisOptions
          & aoType .~ TimeAxis
          & aoTime . toParser .~ "x"
          & aoTime . toTooltipFormat .~ "YYYY-MM-DD HH:mm:ss"
          & aoId .~ "simple"
      ]
    & chartData . cdDatasets . ix 0 . dsXAxisId .~ "simple"
    & chartData . cdXLabels .~ fmap (fst . processEntry) entries
  where
    asEpoch date = read $ formatTime defaultTimeLocale "%s" date :: Int

    processEntry ProgressPoint {..}
      = let magnitude = fromJust $ fmap (\dur -> ppBpm * ppHitsPerBeat * dur `div` 60) ppDuration
              <|> fmap (\Measure {..} -> ppHitsPerBeat * measureLength * measureCount) ppMeasure
        -- moment.js expects millis since epoch
        in (pack . show $ asEpoch ppDate * 1000, magnitude)

makeProgressChart :: Key Exercise -> Handler (Maybe Widget)
makeProgressChart exerciseKey = do
  entries <- getEntriesForExercise exerciseKey
  return $ if null entries
    then Nothing
    else Just $ exerciseProgressChart entries

getFirstExercise :: Handler [(Value (Key Exercise))]
getFirstExercise = runDB $
  select $ from $ \ex -> do
    orderBy [asc (ex ^. ExerciseId)]
    limit 1
    return (ex ^. ExerciseId)

displayProgressPage :: Handler Html
displayProgressPage = do
  ((progressFormRes, progressFormView), _) <- runFormGet progressForm
  maybeGraph <- case progressFormRes of
    FormSuccess exerciseKey -> makeProgressChart exerciseKey
    FormMissing -> do
      maybeKey <- getFirstExercise
      case maybeKey of
        [] -> return Nothing
        Value exerciseKey:_ -> makeProgressChart exerciseKey
    _ -> return Nothing
  Just currentRoute <- getCurrentRoute
  let diaryTabs = $(whamletFile "templates/diary/diary-tabs.hamlet")
  defaultLayout $ do
    setTitle "Progress"
    $(widgetFile "diary/progress")

getProgressR :: Handler Html
getProgressR = displayProgressPage