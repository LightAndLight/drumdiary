{-# language OverloadedStrings #-}

module Handler.Diary.NewEntry where

import           Database.Esqueleto
import           Import             hiding ((==.), on, Value)

import qualified Yesod.Form.Bulma.Fields as B
import Field
import Data.Text.Read

data NewEntry
  = DurationEntry
  { neExercise :: Key Exercise
  , neDate :: UTCTime
  , neBpm :: Int
  , neSubdivision :: Key Subdivision
  , neDuration :: Int
  }
  | MeasureEntry
  { neExercise :: Key Exercise
  , neDate :: UTCTime
  , neBpm :: Int
  , neSubdivision :: Key Subdivision
  , neMeasureCount :: Int
  , neMeasureLength :: Int
  }

data EntryTypeResult
  = DurationResult { etrDuration :: Int }
  | MeasureResult { etrCount :: Int, etrLength :: Int }

entryTypeField :: (RenderMessage (HandlerSite m ) FormMessage, Monad m) => Field m EntryTypeResult
entryTypeField
  = Field
  { fieldParse = \rawVals _ ->
      return $ case rawVals of
        "duration":minutesValue:secondsValue:_ -> case decimal minutesValue of
          Right (minutesRes, "") -> case decimal secondsValue of
            Right (secondsRes, "") -> Right . Just $ DurationResult (minutesRes * 60 + secondsRes)
            _ -> Left $ SomeMessage $ MsgInvalidInteger secondsValue
          _ -> Left $ SomeMessage $ MsgInvalidInteger minutesValue
        "measures":countRes:lenRes:_ -> case decimal countRes of
          Right (countValue, "") -> case decimal lenRes of
            Right (lenValue, "") -> Right . Just $ MeasureResult countValue lenValue
            _ -> Left $ SomeMessage $ MsgInvalidInteger lenRes
          _ -> Left $ SomeMessage $ MsgInvalidInteger countRes
        x:_ -> Left $ SomeMessage (x <> "is not a valid choice. must be duration or measures" :: Text)
        _ -> Left $ SomeMessage ("must submit data for this form" :: Text)
  , fieldView = \theId name attrs valid required -> $(widgetFile "diary/new-entry/measures-duration-field")
  , fieldEnctype = UrlEncoded
  }

entryForm :: Html -> MForm Handler (FormResult NewEntry, Widget)
entryForm fragment = do
  (exRes, exView) <- mreq (selectField $ optionsPersist [] [] exerciseName) "Exercise" Nothing
  (entryTypeRes, entryTypeView) <- mreq entryTypeField "" Nothing
  (subRes, subView) <- mreq (selectField $ optionsPersistKey [] [Asc SubdivisionOrder] subdivisionDisplay) "Subdivision" Nothing
  let bpmSettings = B.bulmaInput ("BPM" :: Text)
  (bpmRes, bpmView) <- mreq intField bpmSettings { fsAttrs = ("min", "0"):fsAttrs bpmSettings } (Just 100)
  now <- liftIO $ getCurrentTime
  let entryRes = case entryTypeRes of
        FormSuccess entryType -> case entryType of
          DurationResult {..} -> DurationEntry <$>
            (entityKey <$> exRes) <*>
            pure now <*>
            bpmRes <*>
            subRes <*>
            pure etrDuration
          MeasureResult {..} -> MeasureEntry <$>
            (entityKey <$> exRes) <*>
            pure now <*>
            bpmRes <*>
            subRes <*>
            pure etrCount <*>
            pure etrLength
        FormMissing -> FormMissing
        FormFailure err -> FormFailure err
  let widget = do
      [whamlet|
        #{fragment}
        <p .control>
          <label .label>#{fvLabel exView}
          <span .select>
            ^{fvInput exView}
        <div .control .is-grouped>
          <p .control>
            <label .label>#{fvLabel subView}
            <span .select>
              ^{fvInput subView}
          <p .control>
            <label .label style="min-width: 5em">#{fvLabel bpmView}
            ^{fvInput bpmView}
        <div .control>
          ^{fvInput entryTypeView}
      |]
  return (entryRes, widget)

insertNewEntry :: NewEntry -> Handler ()
insertNewEntry DurationEntry {..}
  = void . runDB $ do
      entryKey <- insert $ Entry neExercise neDate DurationEntryType neBpm neSubdivision
      insert $ Duration entryKey neDuration
insertNewEntry MeasureEntry {..}
  = void . runDB $ do
      entryKey <- insert $ Entry neExercise neDate MeasureEntryType neBpm neSubdivision
      insert $ Measure entryKey neMeasureCount neMeasureLength

displayNewEntryPage :: Bool -> Widget -> Handler Html
displayNewEntryPage success entryFormWidget = do
  Just currentRoute <- getCurrentRoute
  let diaryTabs = $(whamletFile "templates/diary/diary-tabs.hamlet")
  defaultLayout $ do
    setTitle "New Entry"
    $(widgetFile "diary/new-entry/new-entry")

getNewEntryR :: Handler Html
getNewEntryR = do
  (entryFormWidget, _) <- generateFormPost entryForm
  displayNewEntryPage False entryFormWidget

postNewEntryR :: Handler Html
postNewEntryR = do
  ((res, entryFormWidget), _) <- runFormPost entryForm
  case res of
    FormSuccess entry -> do
      insertNewEntry entry
      displayNewEntryPage True entryFormWidget
    _ -> displayNewEntryPage False entryFormWidget
