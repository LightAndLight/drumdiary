module Field where

import           ClassyPrelude.Yesod
import           Database.Persist.Sql

data EntryType = DurationEntryType | MeasureEntryType deriving (Eq, Show)

instance PersistField EntryType where
  toPersistValue DurationEntryType = PersistText "duration"
  toPersistValue MeasureEntryType = PersistText "measure"

  fromPersistValue (PersistText "duration") = Right DurationEntryType
  fromPersistValue (PersistText "measure") = Right MeasureEntryType
  fromPersistValue x = Left . pack $ "Expected EntryType, received: " ++ show x

instance PersistFieldSql EntryType where
  sqlType _ = SqlString
