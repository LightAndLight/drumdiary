{ mkDerivation, aeson, base, blaze-html, bytestring
, case-insensitive, classy-prelude, classy-prelude-conduit
, classy-prelude-yesod, conduit, containers, data-default
, directory, esqueleto, fast-logger, file-embed, hjsmin, hspec
, http-conduit, lens, monad-control, monad-logger, persistent
, persistent-postgresql, persistent-template, resourcet, safe
, shakespeare, stdenv, template-haskell, text, time, transformers
, unordered-containers, vector, wai, wai-extra, wai-logger, warp
, yaml, yesod, yesod-auth, yesod-core, yesod-form, yesod-static
, yesod-test
}:
mkDerivation {
  pname = "drumdiary";
  version = "0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base blaze-html bytestring case-insensitive classy-prelude
    classy-prelude-conduit classy-prelude-yesod conduit containers
    data-default directory esqueleto fast-logger file-embed hjsmin
    http-conduit lens monad-control monad-logger persistent
    persistent-postgresql persistent-template safe shakespeare
    template-haskell text time unordered-containers vector wai
    wai-extra wai-logger warp yaml yesod yesod-auth yesod-core
    yesod-form yesod-static
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [
    aeson base classy-prelude classy-prelude-yesod hspec monad-logger
    persistent persistent-postgresql resourcet shakespeare transformers
    yesod yesod-auth yesod-core yesod-test
  ];
  postInstall = "cp -pR static $out/";
  doCheck = false;
  license = stdenv.lib.licenses.gpl3;
}
