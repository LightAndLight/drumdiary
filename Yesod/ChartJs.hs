{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Yesod.ChartJs where

import           Data.Aeson
import           Data.Text
import           Prelude
import           Text.Julius
import           Yesod
import Data.Monoid
import Control.Lens hiding ((.=))
import Data.Maybe

opt :: (KeyValue kv, ToJSON a) => Text -> Maybe a -> [kv] -> [kv]
opt _ Nothing pairs = pairs
opt label (Just value) pairs = (label .= value) : pairs

class YesodChartJs site where
  chartJsLocation :: site -> Text
  chartJsLocation _ = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"

data Color = Color { red :: Int, green :: Int, blue :: Int, alpha :: Int } deriving (Eq, Show)

instance ToJSON Color where
  toJSON (Color r g b a) = toJSON $ "rgba(" <> intercalate "," (fmap (pack . show) [r,g,b,a]) <> ")"

data Dataset a
  = Dataset
  { _dsLabel :: Text
  , _dsXAxisId :: Maybe Text
  , _dsYAxisId :: Maybe Text
  , _dsData  :: [a]
  , _dsFill :: Maybe Bool
  , _dsBackgroundColor :: Maybe Color
  , _dsBorderColor :: Maybe Color
  , _dsLineTension :: Maybe Int
  } deriving (Eq, Show)

defaultDataSet :: Dataset a
defaultDataSet
  = Dataset
  { _dsLabel = ""
  , _dsData = []
  , _dsXAxisId = Nothing
  , _dsYAxisId = Nothing
  , _dsFill = Nothing
  , _dsBackgroundColor = Nothing
  , _dsBorderColor = Nothing
  , _dsLineTension = Nothing
  }

dsLabel :: Lens' (Dataset a) Text
dsLabel = lens _dsLabel $ \ds l -> ds { _dsLabel = l }

dsData :: Lens (Dataset a) (Dataset b) [a] [b]
dsData = lens _dsData $ \ds d -> ds { _dsData = d }

dsFill :: Lens (Dataset a) (Dataset a) (Maybe Bool) Bool
dsFill = lens _dsFill $ \ds dsf -> ds { _dsFill = Just dsf }

dsXAxisId :: Lens (Dataset a) (Dataset a) (Maybe Text) Text
dsXAxisId = lens _dsXAxisId $ \ds dsid -> ds { _dsXAxisId = Just dsid }

dsYAxisId :: Lens (Dataset a) (Dataset a) (Maybe Text) Text
dsYAxisId = lens _dsYAxisId $ \ds dsid -> ds { _dsYAxisId = Just dsid }

dsBackgroundColor :: Lens (Dataset a) (Dataset a) (Maybe Color) Color
dsBackgroundColor
  = lens
    _dsBackgroundColor $
    \ds dsbg -> ds { _dsBackgroundColor = Just dsbg }

dsBorderColor :: Lens (Dataset a) (Dataset a) (Maybe Color) Color
dsBorderColor
  = lens
    _dsBorderColor $
    \ds dsbc -> ds { _dsBorderColor = Just dsbc }

dsLineTension :: Lens (Dataset a) (Dataset a) (Maybe Int) Int
dsLineTension
  = lens
    _dsLineTension $
    \ds dslt -> ds { _dsLineTension = Just dslt }

instance ToJSON a => ToJSON (Dataset a) where
  toJSON Dataset {..} = object $
    [ "label" .= _dsLabel
    , "data" .= _dsData
    ]
    & opt "fill" _dsFill
    & opt "backgroundColor" _dsBackgroundColor
    & opt "borderColor" _dsBorderColor
    & opt "lineTension" _dsLineTension
    & opt "xAxisId" _dsXAxisId
    & opt "yAxisId" _dsYAxisId

data ChartType = LineChart deriving (Eq, Show)

instance ToJSON ChartType where
  toJSON LineChart = "line"

data ChartData a
  = ChartData
    { _cdLabels :: Maybe [Text]
    , _cdXLabels :: Maybe [Text]
    , _cdYLabels :: Maybe [Text]
    , _cdDatasets   :: [Dataset a]
    } deriving (Eq, Show)

emptyChartData :: ChartData a
emptyChartData
  = ChartData
  { _cdLabels = Nothing
  , _cdDatasets = []
  , _cdXLabels = Nothing
  , _cdYLabels = Nothing
  }

cdLabels :: Lens (ChartData a) (ChartData a) (Maybe [Text]) [Text]
cdLabels = lens _cdLabels (\cd cdl -> cd { _cdLabels = Just cdl })

cdXLabels :: Lens (ChartData a) (ChartData a) (Maybe [Text]) [Text]
cdXLabels = lens _cdXLabels (\cd cdxl -> cd { _cdXLabels = Just cdxl })

cdYLabels :: Lens (ChartData a) (ChartData a) (Maybe [Text]) [Text]
cdYLabels = lens _cdYLabels (\cd cdyl -> cd { _cdYLabels = Just cdyl })

cdDatasets :: Lens (ChartData a) (ChartData b) [Dataset a] [Dataset b]
cdDatasets = lens _cdDatasets (\cd cdds -> cd { _cdDatasets = cdds })

instance ToJSON a => ToJSON (ChartData a) where
  toJSON ChartData {..} = object $
    [ "datasets" .= _cdDatasets ]
    & opt "labels" _cdLabels
    & opt "xLabels" _cdXLabels
    & opt "yLabels" _cdYLabels

data Position = T | B | L | R deriving (Eq, Show)

instance ToJSON Position where
  toJSON T = "top"
  toJSON B = "bottom"
  toJSON L = "left"
  toJSON R = "right"

data LegendOptions
  = LegendOptions
  { _loDisplay :: Bool
  , _loPosition :: Position
  , _loFullWidth :: Bool
  , _loReverse :: Bool
  } deriving (Eq, Show)

defaultLegendOptions :: LegendOptions
defaultLegendOptions
  = LegendOptions
  { _loDisplay = True
  , _loPosition = T
  , _loFullWidth = True
  , _loReverse = False
  }

makeLenses ''LegendOptions

instance ToJSON LegendOptions where
  toJSON LegendOptions {..} = object
    [ "display" .= _loDisplay
    , "position" .= _loPosition
    , "fullWidth" .= _loFullWidth
    , "reverse" .= _loReverse
    ]

data TimeUnit
  = Millisecond
  | Second
  | Minute
  | Hour
  | Day
  | Week
  | Month
  | Quarter
  | Year
  deriving (Eq, Show)

instance ToJSON TimeUnit where
  toJSON u = toJSON $ case u of
    Millisecond -> ("millisecond" :: Text)
    Second -> ("second" :: Text)
    Minute -> ("minute" :: Text)
    Hour -> ("hour" :: Text)
    Day -> ("day" :: Text)
    Week -> ("week" :: Text)
    Month -> ("month" :: Text)
    Quarter -> ("quarter" :: Text)
    Year -> ("year" :: Text)

data DisplayFormats
  = DisplayFormats
  { _dfMillisecond :: Maybe Text
  , _dfSecond :: Maybe Text
  , _dfMinute :: Maybe Text
  , _dfHour :: Maybe Text
  , _dfDay :: Maybe Text
  , _dfWeek :: Maybe Text
  , _dfMonth :: Maybe Text
  , _dfQuarter :: Maybe Text
  , _dfYear :: Maybe Text
  } deriving (Eq, Show)

defaultDisplayFormats :: DisplayFormats
defaultDisplayFormats
  = DisplayFormats
  { _dfMillisecond = Nothing
  , _dfSecond = Nothing
  , _dfMinute = Nothing
  , _dfHour = Nothing
  , _dfDay = Nothing
  , _dfWeek = Nothing
  , _dfMonth = Nothing
  , _dfQuarter = Nothing
  , _dfYear = Nothing
  }

dfMillisecond :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfMillisecond = lens _dfMillisecond $ \a b -> a { _dfMillisecond = Just b }

dfSecond :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfSecond = lens _dfSecond $ \a b -> a { _dfSecond = Just b }

dfMinute :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfMinute = lens _dfMinute $ \a b -> a { _dfMinute = Just b }

dfHour :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfHour = lens _dfHour $ \a b -> a { _dfHour = Just b }

dfDay :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfDay = lens _dfDay $ \a b -> a { _dfDay = Just b }

dfWeek :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfWeek = lens _dfWeek $ \a b -> a { _dfWeek = Just b }

dfMonth :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfMonth = lens _dfMonth $ \a b -> a { _dfMonth = Just b }

dfQuarter :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfQuarter = lens _dfQuarter $ \a b -> a { _dfQuarter = Just b }

dfYear :: Lens DisplayFormats DisplayFormats (Maybe Text) Text
dfYear = lens _dfYear $ \a b -> a { _dfYear = Just b }

instance ToJSON DisplayFormats where
  toJSON DisplayFormats {..} = object $
    []
    & opt "millisecond" _dfMillisecond
    & opt "second" _dfSecond
    & opt "minute" _dfMinute
    & opt "hour" _dfHour
    & opt "day" _dfDay
    & opt "week" _dfWeek
    & opt "month" _dfMonth
    & opt "quarter" _dfQuarter
    & opt "year" _dfYear

data TimeOptions
  = TimeOptions
  { _toParser :: Maybe Text
  , _toUnit :: Maybe TimeUnit
  , _toDisplayFormats :: Maybe DisplayFormats
  , _toTooltipFormat :: Text
  } deriving (Eq, Show)

defaultTimeOptions :: TimeOptions
defaultTimeOptions
  = TimeOptions
  { _toParser = Nothing
  , _toUnit = Nothing
  , _toDisplayFormats = Nothing
  , _toTooltipFormat = ""
  }

toParser :: Lens TimeOptions TimeOptions (Maybe Text) Text
toParser = lens _toParser $ \t p -> t { _toParser = Just p }

toUnit :: Lens TimeOptions TimeOptions (Maybe TimeUnit) TimeUnit
toUnit = lens _toUnit $ \t u -> t { _toUnit = Just u }

toDisplayFormats :: Lens' TimeOptions DisplayFormats
toDisplayFormats = lens (fromMaybe defaultDisplayFormats . _toDisplayFormats) $ \t df -> t { _toDisplayFormats = Just df }

toTooltipFormat :: Lens' TimeOptions Text
toTooltipFormat = lens _toTooltipFormat $ \t tf -> t { _toTooltipFormat = tf }

instance ToJSON TimeOptions where
  toJSON TimeOptions {..} = object $
    [ "tooltipFormat" .= _toTooltipFormat ]
    & opt "parser" _toParser
    & opt "unit" _toUnit
    & opt "displayFormats" _toDisplayFormats

data AxisType = TimeAxis | CategoryAxis deriving (Eq, Show)

instance ToJSON AxisType where
  toJSON TimeAxis = "time"
  toJSON CategoryAxis = "category"

data AxisOptions
  = AxisOptions
  { _aoType :: AxisType
  , _aoTime :: Maybe TimeOptions
  , _aoId :: Maybe Text
  } deriving (Eq, Show)

defaultAxisOptions :: AxisOptions
defaultAxisOptions
  = AxisOptions
  { _aoType = CategoryAxis
  , _aoTime = Nothing
  , _aoId = Nothing
  }

aoType :: Lens' AxisOptions AxisType
aoType = lens _aoType $ \ao aot -> ao { _aoType = aot }

aoTime :: Lens' AxisOptions TimeOptions
aoTime
  = lens
    (fromMaybe defaultTimeOptions . _aoTime) $
    \ao t -> ao { _aoTime = Just t }

aoId :: Lens AxisOptions AxisOptions (Maybe Text) Text
aoId = lens _aoId $ \ao aoid -> ao { _aoId = Just aoid }

instance ToJSON AxisOptions where
  toJSON AxisOptions {..} = object $
    [ "type" .= _aoType
    ]
    & opt "time" _aoTime
    & opt "id" _aoId

data ScalesOptions
  = ScalesOptions
  { _soXAxes :: Maybe [AxisOptions]
  } deriving (Eq, Show)

defaultScalesOptions :: ScalesOptions
defaultScalesOptions = ScalesOptions { _soXAxes = Nothing }

soXAxes :: Lens' ScalesOptions [AxisOptions]
soXAxes
  = lens
    (fromMaybe [] . _soXAxes) $
    \ao so -> ao { _soXAxes = Just so }

instance ToJSON ScalesOptions where
  toJSON ScalesOptions {..} = object $
    []
    & opt "xAxes" _soXAxes

data ChartOptions
  = ChartOptions
    { _coLegend :: Maybe LegendOptions
    , _coScales :: Maybe ScalesOptions
    } deriving (Eq, Show)

defaultChartOptions :: ChartOptions
defaultChartOptions = ChartOptions { _coLegend = Nothing, _coScales = Nothing }

coLegend :: Lens' ChartOptions LegendOptions
coLegend
  = lens
    (fromMaybe defaultLegendOptions . _coLegend) $
    \co col -> co { _coLegend = Just col }

coScales :: Lens' ChartOptions ScalesOptions
coScales
  = lens
    (fromMaybe defaultScalesOptions . _coScales) $
    \co s -> co { _coScales = Just s }

instance ToJSON ChartOptions where
  toJSON ChartOptions {..} = object $
    []
    & opt "legend" _coLegend
    & opt "scales" _coScales

data Chart a
  = Chart
  { _chartType :: Maybe ChartType
  , _chartData :: Maybe (ChartData a)
  , _chartOptions :: Maybe ChartOptions
  } deriving (Eq, Show)

emptyChart :: Chart a
emptyChart
  = Chart
  { _chartType = Nothing
  , _chartData = Nothing
  , _chartOptions = Nothing
  }

chartType :: Lens (Chart a) (Chart a) (Maybe ChartType) ChartType
chartType = lens _chartType $ \c ct -> c { _chartType = Just ct }

chartData :: Lens (Chart a) (Chart b) (ChartData a) (ChartData b)
chartData
  = lens
    (fromMaybe emptyChartData . _chartData) $
    \c cd -> c { _chartData = Just cd }

chartOptions :: Lens' (Chart a) ChartOptions
chartOptions
  = lens
    (fromMaybe defaultChartOptions . _chartOptions) $
    \c co -> c { _chartOptions = Just co }

instance ToJSON a => ToJSON (Chart a) where
  toJSON Chart {..} = object $
    []
    & opt "type" _chartType
    & opt "data" _chartData
    & opt "options" _chartOptions

simpleLineChart :: [(Text, Int)] -> Color -> Color -> Bool -> Chart Int
simpleLineChart dataset lineColor bgColor fill
  = emptyChart
    & chartType .~ LineChart
    & chartOptions . coLegend . loDisplay .~ False
    & chartData .~
      (emptyChartData
      & cdLabels .~ fmap fst dataset
      & cdDatasets .~
        [ defaultDataSet
          & dsData .~ fmap snd dataset
          & dsFill .~ fill
          & dsBackgroundColor .~ bgColor
          & dsBorderColor .~ lineColor
          & dsLineTension .~ 0
        ])

chart :: (ToJSON a, MonadWidget m, MonadHandler m, HandlerSite m ~ site, YesodChartJs site) => Chart a -> m ()
chart c = do
  site <- getYesod
  let chartjs = chartJsLocation site
  addScriptRemote chartjs
  divId <- newIdent
  toWidget [julius|
    var chart = new Chart(
      #{toJSON divId},
      #{toJSON c}
    );
  |]
  toWidgetBody [hamlet|
    <canvas id=#{divId}>
  |]
