module Yesod.Form.Bulma.Fields where

import qualified Yesod.Form.Fields as Y
import Yesod.Form.Types
import Data.Either
import Import

addClass :: Text -> FieldSettings site -> FieldSettings site
addClass klass settings
  = settings
  { fsAttrs =
      if "class" `elem` fmap fst (fsAttrs settings)
        then fmap mapAttr $ fsAttrs settings
        else ("class", klass):fsAttrs settings
  }
  where
    mapAttr (attrName,attrValue)
      | attrName == "class" = (attrName, klass <> " " <> attrValue)
      | otherwise = (attrName, attrValue)

bulmaInput :: RenderMessage site msg => msg -> FieldSettings site
bulmaInput msg = FieldSettings (SomeMessage msg) Nothing Nothing Nothing [("class", "input")]

bulmaTextarea :: RenderMessage site msg => msg -> FieldSettings site
bulmaTextarea msg = FieldSettings (SomeMessage msg) Nothing Nothing Nothing [("class", "textarea")]

textareaField :: Monad m => RenderMessage (HandlerSite m) FormMessage => Field m Y.Textarea
textareaField
  = Y.textareaField
    { fieldView = \ident name attrs res req -> do
      [whamlet|
        <textarea *{attrs} .textarea name="#{name}" id="#{ident}" value="#{either id Y.unTextarea res}">
      |]
    }
