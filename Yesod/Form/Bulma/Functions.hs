module Yesod.Form.Bulma.Functions where

import Prelude (Monad, return)
import Yesod.Form.Types
import Yesod.Form.Functions
import Yesod.Core

renderBulma :: Monad m => FormRender m a
renderBulma aform fragment = do
  (res, views') <- aFormToForm aform
  let views = views' []
  let widget = [whamlet|
    #{fragment}
    $forall view <- views
      <p .control>
        <label .label>#{fvLabel view}
        ^{fvInput view}
        $maybe tt <- fvTooltip view
          <span .help .is-success>#{tt}
        $maybe err <- fvErrors view
          <span .help .is-danger>#{err}
  |]
  return (res, widget)
