{-# LANGUAGE OverloadedStrings #-}

module Fixtures where

import           Data.Foldable          (traverse_)
import           Database.Persist.Class
import           Database.Persist.Sql
import           Model
import           Prelude                (uncurry)

subdivisionsFixture = traverse_ (uncurry repsert)
  [ (toSqlKey 0, Subdivision 0 "1/8" 2)
  , (toSqlKey 1, Subdivision 1 "1/16" 4)
  , (toSqlKey 2, Subdivision 2 "1/8 triplet" 3)
  , (toSqlKey 3, Subdivision 3 "1/16 triplet" 6)
  , (toSqlKey 4, Subdivision 5 "1/8 quintuplet" 5)
  ]
