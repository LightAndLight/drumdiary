{-# LANGUAGE PackageImports #-}
import "drumdiary" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
