let
  vbox = 
    { deployment.targetEnv = "virtualbox";
      deployment.virtualbox.memorySize = 256;
      deployment.virtualbox.vcpu = 2;
      deployment.virtualbox.headless = true;
    };
in
{
  server = vbox;
  database = vbox;
}
