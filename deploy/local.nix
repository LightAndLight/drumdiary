let
  pg_port = 5432;
  webapp_port = 3000;
in

{
  network.description = "drumdiary with local test database";

  database =
    { config, pkgs, ... }:
    { services.postgresql.enable = true;
      services.postgresql.package = pkgs.postgresql96;
      services.postgresql.port = pg_port;
      services.postgresql.enableTCPIP = true;
      services.postgresql.authentication = ''
        host all all server trust
      '';
      networking.firewall.allowedTCPPorts = [ pg_port ];
    };

  server =
    { config, pkgs, ... }:
      let
        drumdiary = import ../default.nix { };
      in
      { 
        environment.systemPackages = [ drumdiary ];
        networking.firewall.allowedTCPPorts = [ 80 pg_port ];

        services.nginx.enable = true;
        services.nginx.virtualHosts = 
          let
            rootServer =
              { locations."/" =
                { proxyPass = "http://localhost:${toString webapp_port}"; };
              };
          in
          { "drumdiary.net" = rootServer;
            "localhost" = rootServer;
          };

        systemd.services.drumdiary =
          { description = "drumdiary web server";

            wantedBy = [ "multi-user.target" ];

            after = [ "network.target" ];

            preStart = ''
              mkdir -p /var/drumdiary
              cp -R ${drumdiary}/static /var/drumdiary
            '';

            serviceConfig = {
              Environment = [
                "STATIC_DIR=/var/drumdiary/static"
                "PORT=${toString webapp_port}"
                "PGHOST=database"
                "PGPORT=${toString pg_port}"
                "PGUSER=root"
                "PGDATABASE=postgres"
              ];
              ExecStart = "${drumdiary}/bin/drumdiary";
              StandardOutput = "syslog";
              StandardError = "syslog";
            };
          };
      };
}
